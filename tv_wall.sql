-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Nov 2019 pada 03.33
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tv_wall`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bg`
--

CREATE TABLE `bg` (
  `id_bg` int(10) NOT NULL,
  `foto` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `bg`
--

INSERT INTO `bg` (`id_bg`, `foto`) VALUES
(9, 'f0f0dd4c-0a6f-4fa6-845b-ace5ef09df84.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jdwal_cuti`
--

CREATE TABLE `jdwal_cuti` (
  `id_jdwal_cuti` int(10) NOT NULL,
  `nama` varchar(100) COLLATE utf8_bin NOT NULL,
  `nip` varchar(100) COLLATE utf8_bin NOT NULL,
  `jabatan` varchar(100) COLLATE utf8_bin NOT NULL,
  `jenis_cuti` varchar(100) COLLATE utf8_bin NOT NULL,
  `alasan_cuti` varchar(100) COLLATE utf8_bin NOT NULL,
  `lama_cuti` varchar(50) COLLATE utf8_bin NOT NULL,
  `alamat_cuti` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `jdwal_cuti`
--

INSERT INTO `jdwal_cuti` (`id_jdwal_cuti`, `nama`, `nip`, `jabatan`, `jenis_cuti`, `alasan_cuti`, `lama_cuti`, `alamat_cuti`) VALUES
(1, 'Randi Perdana Arman', '123456789', 'IT', 'Sakit', 'Sakit', '2 minggu', 'Batam'),
(2, 'Ardiansyah', '654321', 'IT', 'Sakit', 'Sakit', '2 Hari', 'Batam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jdwal_kaban`
--

CREATE TABLE `jdwal_kaban` (
  `id_jdwal_kaban` int(10) NOT NULL,
  `hari` varchar(20) COLLATE utf8_bin NOT NULL,
  `tgl` date NOT NULL,
  `jam` varchar(20) COLLATE utf8_bin NOT NULL,
  `kegiatan` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `jdwal_kaban`
--

INSERT INTO `jdwal_kaban` (`id_jdwal_kaban`, `hari`, `tgl`, `jam`, `kegiatan`) VALUES
(1, 'jumat', '2019-10-30', '10.00', 'Rapat'),
(2, 'kamis', '2019-10-31', '10.00', 'Rapat bersama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jdwal_kegiatan`
--

CREATE TABLE `jdwal_kegiatan` (
  `id_jdwal_kegiatan` int(10) NOT NULL,
  `hari` varchar(20) COLLATE utf8_bin NOT NULL,
  `tgl` date NOT NULL,
  `jam` varchar(20) COLLATE utf8_bin NOT NULL,
  `kegiatan` varchar(200) COLLATE utf8_bin NOT NULL,
  `ket` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `jdwal_kegiatan`
--

INSERT INTO `jdwal_kegiatan` (`id_jdwal_kegiatan`, `hari`, `tgl`, `jam`, `kegiatan`, `ket`) VALUES
(1, 'Jumat', '2019-11-01', '11.00', 'Kunjungan Kerja dari BPKAD Palembang', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur`
--

CREATE TABLE `struktur` (
  `id_struktur` int(10) NOT NULL,
  `foto` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_log`
--

CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL,
  `log_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_log`
--

INSERT INTO `tabel_log` (`log_id`, `log_time`, `log_user`, `log_tipe`, `log_desc`) VALUES
(1, '2019-10-21 20:31:45', 'ardi', 2, 'Tambah Data Welcome'),
(2, '2019-10-21 22:50:29', 'ardi', 4, 'Hapus Data Welcome'),
(3, '2019-10-21 22:57:11', 'ardi', 4, 'Hapus Data Welcome'),
(4, '2019-10-21 22:57:18', 'ardi', 2, 'Tambah Data Welcome'),
(5, '2019-10-21 22:59:49', 'ardi', 4, 'Hapus Data Welcome'),
(6, '2019-10-21 23:02:26', 'ardi', 2, 'Tambah Data Welcome'),
(7, '2019-10-21 23:05:34', 'ardi', 4, 'Hapus Data Welcome'),
(8, '2019-10-21 23:05:40', 'ardi', 2, 'Tambah Data Welcome'),
(9, '2019-10-21 23:06:07', 'ardi', 4, 'Hapus Data Welcome'),
(10, '2019-10-21 23:06:23', 'ardi', 2, 'Tambah Data Welcome'),
(11, '2019-10-21 23:08:10', 'ardi', 4, 'Hapus Data Welcome'),
(12, '2019-10-21 23:10:16', 'ardi', 2, 'Tambah Data Welcome'),
(13, '2019-10-21 23:17:07', 'ardi', 4, 'Hapus Data Welcome'),
(14, '2019-10-21 23:17:20', 'ardi', 2, 'Tambah Data Welcome'),
(15, '2019-10-22 14:35:24', 'ardi', 4, 'Hapus Data Welcome'),
(16, '2019-10-22 14:35:31', 'ardi', 2, 'Tambah Data Welcome'),
(17, '2019-10-22 17:04:34', 'ardi', 2, 'Tambah Data Agenda BPKAD'),
(18, '2019-10-22 17:08:28', 'ardi', 3, 'Edit Data Agenda BPKAD'),
(19, '2019-10-22 17:08:33', 'ardi', 4, 'Hapus Data Agenda BPKAD'),
(20, '2019-10-22 22:35:24', 'ardi', 2, 'Tambah Data Agenda Kaban'),
(21, '2019-10-22 22:37:48', 'ardi', 3, 'Edit Data Agenda Kaban'),
(22, '2019-10-22 22:38:40', 'ardi', 3, 'Edit Data Agenda Kaban'),
(23, '2019-10-22 22:38:46', 'ardi', 3, 'Edit Data Agenda Kaban'),
(24, '2019-10-22 22:43:16', 'ardi', 3, 'Edit Data Agenda Kaban'),
(25, '2019-10-22 22:43:59', 'ardi', 3, 'Edit Data Agenda Kaban'),
(26, '2019-10-22 22:44:10', 'ardi', 3, 'Edit Data Agenda Kaban'),
(27, '2019-10-22 22:44:17', 'ardi', 3, 'Edit Data Agenda Kaban'),
(28, '2019-10-22 22:44:21', 'ardi', 4, 'Hapus Data Agenda Kaban');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_bpkad`
--

CREATE TABLE `user_bpkad` (
  `id_user` int(10) NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(100) COLLATE utf8_bin NOT NULL,
  `level` varchar(10) COLLATE utf8_bin NOT NULL,
  `bagian` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user_bpkad`
--

INSERT INTO `user_bpkad` (`id_user`, `username`, `pwd`, `level`, `bagian`) VALUES
(1, 'ardi', 'ardi', 'Operator', 'Anggaran'),
(3, 'ibnu', 'ibnu', 'Operator', 'Aset'),
(4, 'ardi', '202cb962ac59075b964b07152d234b70', '', ''),
(5, 'randi', 'ec1a08ca25857e260784856b3556804d', 'Admin', 'Programmer');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bg`
--
ALTER TABLE `bg`
  ADD PRIMARY KEY (`id_bg`);

--
-- Indeks untuk tabel `jdwal_cuti`
--
ALTER TABLE `jdwal_cuti`
  ADD PRIMARY KEY (`id_jdwal_cuti`);

--
-- Indeks untuk tabel `jdwal_kaban`
--
ALTER TABLE `jdwal_kaban`
  ADD PRIMARY KEY (`id_jdwal_kaban`);

--
-- Indeks untuk tabel `jdwal_kegiatan`
--
ALTER TABLE `jdwal_kegiatan`
  ADD PRIMARY KEY (`id_jdwal_kegiatan`);

--
-- Indeks untuk tabel `struktur`
--
ALTER TABLE `struktur`
  ADD PRIMARY KEY (`id_struktur`);

--
-- Indeks untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indeks untuk tabel `user_bpkad`
--
ALTER TABLE `user_bpkad`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bg`
--
ALTER TABLE `bg`
  MODIFY `id_bg` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `jdwal_cuti`
--
ALTER TABLE `jdwal_cuti`
  MODIFY `id_jdwal_cuti` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `jdwal_kaban`
--
ALTER TABLE `jdwal_kaban`
  MODIFY `id_jdwal_kaban` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jdwal_kegiatan`
--
ALTER TABLE `jdwal_kegiatan`
  MODIFY `id_jdwal_kegiatan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `struktur`
--
ALTER TABLE `struktur`
  MODIFY `id_struktur` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `user_bpkad`
--
ALTER TABLE `user_bpkad`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
