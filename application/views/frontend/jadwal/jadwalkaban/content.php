	<div class="card bg-light text-dark">
        <div class="card-body">
           <table id="uud" class="display table table-bordered table-striped" style="width: 100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Hari</th>
						<th>Tanggal</th>
						<th>Jam</th>
                        <th>Kegiatan</th>
					</tr>
				</thead>
				<tbody>
					<?php
                    $query = $this->db->query('SELECT * FROM jdwal_kaban');
                        $no=1;
                        foreach ($query->result() as $row)
                        {?>	
                        <tr>
                            <td><?php echo $no;?></td>
                        	<td><?php echo $row->hari;?></td>
                            <td><?php echo date('d-F-Y',strtotime($row->tgl));?></td>
                            <td><?php echo $row->jam; ?></td>
                            <td><?php echo $row->kegiatan;?></td>
						</tr>
						<?php
							$no++;}
							?>
				</tbody>
			</table>

        </div>
    </div>



