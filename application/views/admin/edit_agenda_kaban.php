<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Agenda Kaban</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Agenda Kaban administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3>EDIT DATA AGENDA KABAN</h3></center>
	 	
			<?php foreach ($agenda as $key => $us) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_agenda_kaban/update' ?>">
					<div class="form-group">
						<label>Hari</label>
						<input type="hidden" name="id_jdwal_kaban" class="form-control" value="<?php echo $us->id_jdwal_kaban?>">
						<input type="text" name="hari" class="form-control" value="<?php echo $us->hari?>">
					</div>

					<div class="form-group">
						<label>Tanggal</label>
						<input type="text" name="tgl" class="form-control" value="<?php echo $us->tgl?>">
					</div>

					<div class="form-group">
						<label>Jam</label>
						<input type="text" name="jam" class="form-control" value="<?php echo $us->jam?>">
					</div>

					<div class="form-group">
						<label>Kegiatan</label>
						<input type="text" name="kegiatan" class="form-control" value="<?php echo $us->kegiatan?>">
					</div>

					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>
</div>
</div>