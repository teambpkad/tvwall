<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Agenda Cuti</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Agenda Cuti administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3>EDIT DATA JADWAL CUTI</h3></center>
	 	
			<?php foreach ($agenda as $key => $us) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_agenda_cuti/update' ?>">
					<div class="form-group">
						<label>Nama</label>
						<input type="hidden" name="id_jdwal_cuti" class="form-control" value="<?php echo $us->id_jdwal_cuti?>">
						<input type="text" name="nama" class="form-control" value="<?php echo $us->nama?>">
					</div>

					<div class="form-group">
						<label>Nip</label>
						<input type="text" name="nip" class="form-control" value="<?php echo $us->nip?>">
					</div>

					<div class="form-group">
						<label>Jabatan</label>
						<input type="text" name="jabatan" class="form-control" value="<?php echo $us->jabatan?>">
					</div>

					<div class="form-group">
						<label>Jenis Cuti</label>
						<input type="text" name="jenis_cuti" class="form-control" value="<?php echo $us->jenis_cuti?>">
					</div>

					<div class="form-group">
						<label>Alasan Cuti</label>
						<input type="text" name="alasan_cuti" class="form-control" value="<?php echo $us->alasan_cuti?>">
					</div>

					<div class="form-group">
						<label>Lama Cuti</label>
						<input type="text" name="lama_cuti" class="form-control" value="<?php echo $us->lama_cuti?>">
					</div>

					<div class="form-group">
						<label>Alamat Cuti</label>
						<input type="text" name="alamat_cuti" class="form-control" value="<?php echo $us->alamat_cuti?>">
					</div>

					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>
</div>
</div>