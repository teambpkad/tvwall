<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Welcome</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Welcome administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

    <div class="content-wrapper">
      <div class="container-fluid">
        <center><h3>EDIT DATA WELCOME</h3></center>
      
        <?php foreach ($b as $key => $pf) : ?>
          <!-- Di lupo mamasuakan enctype="multipart/form-data" di form -->
          <form method="post"  action="<?php echo base_url(). 'admin/data_bg/update' ?>" enctype="multipart/form-data">
           
            <div class="form-group">
              <label>Foto lama</label>
              <input type="text" name="old_foto" value="<?php echo($pf->foto)?>" readonly>
              <img src="<?php echo base_url('./uploads/bg/')?><?php echo $pf->foto?>" width="100">
            </div>

            <div class="form-group">
              <label>Update Foto Jabatan</label>
              <input type="file" id="new_foto" name="new_foto" class="form-control">
            </div>  
            
            <button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
          </form>

        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>