<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <table id="log-table" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <th class="center">Date & Time</th>
                              <th class="center">User</th>
                              <th Class="center">Aktivitas Administrator</th>
                            </tr>
                          </thead>

                          <tbody>
                            <?php foreach ($log as $key => $lg):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $lg->log_time ?></td>
                                <td><?php  echo $lg->log_user ?></td>
                                <td><?php  echo $lg->log_desc ?></td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                </table>
                       
                    </div>

                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>