<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Surat Edaran</li>
      </ul><!-- /.breadcrumb -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Surat Edaran bpkad-batam
          </small>
        </h1>
      </div>
      <!-- /.page-header -->
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#tambahsurat">
                          <i class="ace-icon fa fa-plus"></i>
                          Surat Edaran
                </button>
            </div>
          </div>

          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                    <!-- div.table-responsive -->
                    <!-- div.dataTables_borderWrap -->
                      <div>
                        <table id="table-user" class="table table-striped table-bordered" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th class="center">No</th>
                              <th class="center">Judul</th>
                              <th class="center">Tanggal</th>
                              <th class="center">Status</th>
                              <th class="Center">Aksi</th>
                            </tr>
                          </thead>

                          <tbody>
                        
                            <?php foreach ($edaran as $key => $us):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $no+$key ?></td>
                                <td><?php  echo $us->judul ?></td>
                                <td><?php  echo $us->tgl ?></td>d>
                                <td><?php  echo $us->status ?></td>d>
                               <td>
                                  <div class="hidden-sm hidden-xs action-buttons">
                                    <a onclick="javascript: return confirm('Anda Yakin Hapus')" class="red" href="<?php echo base_url('admin/data_surat_edaran/hapus/'.$us->id_surat_edaran)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                  </div>

                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_surat_edaran/hapus/'.$us->id_surat_edaran)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                            </td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
               </div>                  
              </div>  
            </div>         
            <!-- Modal -->
            <div class="modal fade" id="tambahsurat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT SURAT EDARAN</h5></center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo base_url(). 'admin/data_surat_edaran/tambah_aksi'?>" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label>Judul</label>
                        <input type="text" name="judul" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Tanggal</label>
                        <input type="date" name="tgl" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>File</label>
                        <input type="file" name="file" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Status Terbit</label>
                        <select type="text" name="status" class="form-control">
                          <option>No</option>
                          <option>Yes</option>
                        </select>
                      </div>
                      
                  </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
<!--page <table></table>-->
<!-- isi content admin/data_user -->
          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->


