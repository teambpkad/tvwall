<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Agenda Cuti</li>
      </ul><!-- /.breadcrumb -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Agenda Cuti administrator bpkad-batam
          </small>
        </h1>
      </div>
      <!-- /.page-header -->
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#tambahuser">
                          <i class="ace-icon fa fa-plus"></i>
                          Agenda Cuti
                </button>
            </div>
          </div>

          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                    <!-- div.table-responsive -->
                    <!-- div.dataTables_borderWrap -->
                      <div>
                        <table id="table-user" class="table table-striped table-bordered" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th class="center">No</th>
                              <th class="center">Nama</th>
                              <th class="center">Nip</th>
                              <th class="center">Jabatan</th>
                              <th Class="Center">Jenis Cuti</th>
                              <th Class="center">Alasan Cuti</th>
                              <th Class="center">Lama Cuti</th>
                              <th Class="center">Alamat Cuti</th>
                              <th class="Center">Aksi</th>
                            </tr>
                          </thead>

                          <tbody>
                        
                            <?php foreach ($agenda as $key => $us):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $no+$key ?></td>
                                <td><?php  echo $us->nama ?></td>
                                <td><?php  echo $us->nip ?></td>
                                <td><?php  echo $us->jabatan ?></td>
                                <td><?php  echo $us->jenis_cuti ?></td>
                                <td><?php  echo $us->alasan_cuti ?></td>
                                <td><?php  echo $us->lama_cuti ?></td>
                                <td><?php  echo $us->alamat_cuti ?></td>
                               <td>
                                  <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="green" href="<?php echo base_url('admin/data_agenda_cuti/edit/'.$us->id_jdwal_cuti)?>">
                                      <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a onclick="javascript: return confirm('Anda Yakin Hapus')" class="red" href="<?php echo base_url('admin/data_agenda_cuti/hapus/'.$us->id_jdwal_cuti)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                  </div>

                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_agenda_cuti/edit/'.$us->id_jdwal_cuti)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                            <span class="green">
                                              <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="<?php echo base_url('admin/data_agenda_cuti/hapus/'.$us->id_jdwal_cuti)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                            </td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
               </div>                  
              </div>  
            </div>         
            <!-- Modal -->
            <div class="modal fade" id="tambahuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT JADWAL CUTI</h5></center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo base_url(). 'admin/data_agenda_cuti/tambah_aksi'?>" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Nip</label>
                        <input type="text" name="nip" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Jabatan</label>
                        <input type="text" name="jabatan" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Jenis Cuti</label>
                        <input type="text" name="jenis_cuti" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Alasan Cuti</label>
                        <input type="text" name="alasan_cuti" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Lama Cuti</label>
                        <input type="text" name="lama_cuti" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Alamat Cuti</label>
                        <input type="text" name="alamat_cuti" class="form-control" required="">
                      </div>
                      
                  </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
<!--page <table></table>-->
<!-- isi content admin/data_user -->
          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->


