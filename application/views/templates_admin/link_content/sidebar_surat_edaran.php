     <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <ul class="nav nav-list">
          <li class="active">
            <a href="<?php echo base_url('admin/dashboard_admin')?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_user')?>">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">Adminstrator</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_bg')?>">
              <i class="menu-icon fa fa-picture-o"></i>
              <span class="menu-text">Welcome</span>
            </a> 

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_agenda_bpkad')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Agenda BPKAD</span>
            </a> 

            <b class="arrow"></b>
          </li>

           <li class="">
            <a href="<?php echo base_url('admin/data_agenda_kaban')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Agenda KABAN</span>
            </a> 

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_agenda_cuti')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Agenda Cuti</span>
            </a> 

            <b class="arrow"></b>
          </li>

          <li class="active">
            <a href="<?php echo base_url('admin/data_surat_edaran')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Surat Edaran</span>
            </a> 

            <b class="arrow"></b>
          </li>

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>