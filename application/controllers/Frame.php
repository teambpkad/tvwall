<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frame extends CI_Controller 
    {

    public function index()
        {
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frame.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
}   
?>
