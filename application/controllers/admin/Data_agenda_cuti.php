<?php
	class Data_agenda_cuti extends CI_Controller
	{

		public function index()
		{
			$data['agenda'] = $this->model_agenda_cuti->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_cuti');
			$this->load->view('admin/data_agenda_cuti',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$nama 		 = $this->input->post('nama');
			$nip         = $this->input->post('nip');
			$jabatan     = $this->input->post('jabatan');
			$jenis_cuti  = $this->input->post('jenis_cuti');
			$alasan_cuti = $this->input->post('alasan_cuti');
			$lama_cuti   = $this->input->post('lama_cuti');
			$alamat_cuti = $this->input->post('alamat_cuti');

			$data = array (
				'nama' 		  => $nama,
				'nip'         => $nip,
				'jabatan'     => $jabatan,
				'jenis_cuti'  => $jenis_cuti,
				'alasan_cuti' => $alasan_cuti,
				'lama_cuti'   => $lama_cuti,
				'alamat_cuti' => $alamat_cuti
			);

		$this->model_agenda_cuti->tambah_agenda($data, 'jdwal_cuti');
		helper_log("add", "Tambah Data Agenda Cuti");
		redirect('admin/data_agenda_cuti');
		}

		public function edit($id)
		{
			$where = array('id_jdwal_cuti' =>$id);
			$data['agenda'] = $this->model_agenda_cuti->edit_agenda($where, 'jdwal_cuti')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_cuti');
			$this->load->view('admin/edit_agenda_cuti',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id      	 = $this->input->post('id_jdwal_cuti');
			$nama 		 = $this->input->post('nama');
			$nip         = $this->input->post('nip');
			$jabatan     = $this->input->post('jabatan');
			$jenis_cuti  = $this->input->post('jenis_cuti');
			$alasan_cuti = $this->input->post('alasan_cuti');
			$lama_cuti   = $this->input->post('lama_cuti');
			$alamat_cuti = $this->input->post('alamat_cuti');

			$data = array(
				'nama' 		  => $nama,
				'nip'         => $nip,
				'jabatan'     => $jabatan,
				'jenis_cuti'  => $jenis_cuti,
				'alasan_cuti' => $alasan_cuti,
				'lama_cuti'   => $lama_cuti,
				'alamat_cuti' => $alamat_cuti
			);
			$where = array (
				'id_jdwal_cuti' =>$id
			);
			$this->model_agenda_cuti->update_data($where, $data, 'jdwal_cuti');
			var_dump($data);
			helper_log("edit", "Edit Data Agenda Cuti");
			redirect('admin/data_agenda_cuti');
		}

		public function hapus($id)
		{
			$where = array('id_jdwal_cuti' => $id);
			$this->model_agenda_cuti->hapus_data($where, 'jdwal_cuti');
			helper_log("hapus", "Hapus Data Agenda Cuti");
			redirect('admin/data_agenda_cuti');
		}
	}
?>