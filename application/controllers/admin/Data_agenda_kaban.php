<?php
	class Data_agenda_kaban extends CI_Controller
	{

		public function index()
		{
			$data['agenda'] = $this->model_agenda_kaban->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_kaban');
			$this->load->view('admin/data_agenda_kaban',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$hari 		 = $this->input->post('hari');
			$tgl         = $this->input->post('tgl');
			$jam         = $this->input->post('jam');
			$kegiatan    = $this->input->post('kegiatan');

			$data = array (
				'hari' 		  => $hari,
				'tgl'         => $tgl,
				'jam'         => $jam,
				'kegiatan'    => $kegiatan
			);

		$this->model_agenda_kaban->tambah_agenda($data, 'jdwal_kaban');
		helper_log("add", "Tambah Data Agenda Kaban");
		redirect('admin/data_agenda_kaban');
		}

		public function edit($id)
		{
			$where = array('id_jdwal_kaban' =>$id);
			$data['agenda'] = $this->model_agenda_kaban->edit_agenda($where, 'jdwal_kaban')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_kaban');
			$this->load->view('admin/edit_agenda_kaban',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id 		 = $this->input->post('id_jdwal_kaban');
			$hari 		 = $this->input->post('hari');
			$tgl   	     = $this->input->post('tgl');
			$jam   	     = $this->input->post('jam');
			$kegiatan    = $this->input->post('kegiatan');

			$data = array(
				'hari' 		  => $hari,
				'tgl'    	  => $tgl,
				'jam'    	  => $jam,
				'kegiatan'    => $kegiatan
			);
			$where = array (
				'id_jdwal_kaban' =>$id
			);
			$this->model_agenda_kaban->update_data($where, $data, 'jdwal_kaban');
			var_dump($data);
			helper_log("edit", "Edit Data Agenda Kaban");
			redirect('admin/data_agenda_kaban');
		}

		public function hapus($id)
		{
			$where = array('id_jdwal_kaban' => $id);
			$this->model_agenda_kaban->hapus_data($where, 'jdwal_kaban');
			helper_log("hapus", "Hapus Data Agenda Kaban");
			redirect('admin/data_agenda_kaban');
		}
	}
?>