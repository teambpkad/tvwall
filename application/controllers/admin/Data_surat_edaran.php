<?php
	class Data_surat_edaran extends CI_Controller{
		public function index()
		{
			$data['edaran'] = $this->model_surat_edaran->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_surat_edaran');
			$this->load->view('admin/data_surat_edaran',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'jpeg|jpg|png|gif';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $judul	    = $this->input->post('judul');
            $tgl        = $this->input->post('tgl');
            $status     = $this->input->post('status');

            $data = array(
                'judul'       => $judul,
                'tgl'     	  => $tgl,
                'file'        => $file_name,                        
                'status'      => $status                        
            );

            $this->model_surat_edaran->tambah_surat($data, 'surat_edaran');
            helper_log("add", "Tambah Data Surat Edaran");
            redirect('admin/data_surat_edaran');
		}

		public function hapus($id)
		{
			$file = $this->model_surat_edaran->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/".$nama_file);
            $hasil = $this->model_surat_edaran->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Surat Edaran");
            redirect('admin/data_surat_edaran');
		}
	}
?>