<?php
	class Data_agenda_bpkad extends CI_Controller
	{

		public function index()
		{
			$data['agenda'] = $this->model_agenda_bpkad->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_bpkad');
			$this->load->view('admin/data_agenda_bpkad',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$hari 		 = $this->input->post('hari');
			$tgl         = $this->input->post('tgl');
			$jam         = $this->input->post('jam');
			$kegiatan    = $this->input->post('kegiatan');
			$ket  		 = $this->input->post('ket');

			$data = array (
				'hari' 		  => $hari,
				'tgl'         => $tgl,
				'jam'         => $jam,
				'kegiatan'    => $kegiatan,
				'ket'   	  => $ket
			);

		$this->model_agenda_bpkad->tambah_agenda($data, 'jdwal_kegiatan');
		helper_log("add", "Tambah Data Agenda BPKAD");
		redirect('admin/data_agenda_bpkad');
		}

		public function edit($id)
		{
			$where = array('id_jdwal_kegiatan' =>$id);
			$data['agenda'] = $this->model_agenda_bpkad->edit_agenda($where, 'jdwal_kegiatan')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_agenda_bpkad');
			$this->load->view('admin/edit_agenda_bpkad',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id      	 = $this->input->post('id_jdwal_kegiatan');
			$hari 		 = $this->input->post('hari');
			$tgl   	     = $this->input->post('tgl');
			$jam   	     = $this->input->post('jam');
			$kegiatan    = $this->input->post('kegiatan');
			$ket  		 = $this->input->post('ket');

			$data = array(
				'hari' 		  => $hari,
				'tgl'    	  => $tgl,
				'jam'    	  => $jam,
				'kegiatan'    => $kegiatan,
				'ket'   	  => $ket
			);
			$where = array (
				'id_jdwal_kegiatan' =>$id
			);
			$this->model_agenda_bpkad->update_data($where, $data, 'jdwal_kegiatan');
			helper_log("edit", "Edit Data Agenda BPKAD");
			redirect('admin/data_agenda_bpkad');
		}

		public function hapus($id)
		{
			$where = array('id_jdwal_kegiatan' => $id);
			$this->model_agenda_bpkad->hapus_data($where, 'jdwal_kegiatan');
			helper_log("hapus", "Hapus Data Agenda BPKAD");
			redirect('admin/data_agenda_bpkad');
		}
	}
?>