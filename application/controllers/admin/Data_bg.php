<?php
	class Data_bg extends CI_Controller{
        
		public function index()
		{
			$data['b'] = $this->model_bg->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_bg');
			$this->load->view('admin/data_bg',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}
 
		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/bg/';
            $config['allowed_types']        = 'jpg|jpeg|png';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            

            $data = array(
                'foto'         	 => $file_name                        
            );

            $this->model_bg->tambah_bg($data, 'bg');
            helper_log("add", "Tambah Data Welcome");
            redirect('admin/data_bg');
		}

		public function edit($id)
		{
			$where = array('id_bg' =>$id);

			$data['b'] = $this->model_bg->edit_bg($where, 'bg')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_bg');
			$this->load->view('admin/edit_bg',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id         = $this->input->post('id_bg');
            $foto_lama  = $this->input->post('old_foto');
            // um tuka jadi ko mudah2an bisa
            $foto_baru  = $_FILES['new_foto']['name'];
 
            $config['upload_path'] = './uploads/bg/';
                $config['allowed_types']= 'jpg|png|jpeg';
                $config['image_library'] = 'GD2';
                $this->load->library('upload', $config);

            if($foto_baru =''){
                if($this->upload->do_upload('old_foto')){
                    $name_file1 = "";
                    $name_file1  =  $this->upload->data('file_name');
                }
               
                
            }else{
                 
                if($this->upload->do_upload('new_foto')){
                    $name_file1 = "";
                    $name_file1  =  $this->upload->data('file_name');
                }
                unlink("./uploads/bg/".$foto_lama);
            }
            $data = array(
                    'foto'       => $name_file1 
                );
                
            $where = array (
                'id_bg' =>$id
            );
            $this->model_bg->update_data($where, $data, 'bg');
            redirect('admin/data_bg');
		}

		public function hapus($id)
		{
			$file = $this->model_bg->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/bg/".$nama_file);
            $hasil = $this->model_bg->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Welcome");
            redirect('admin/data_bg');
		}
	}
?>