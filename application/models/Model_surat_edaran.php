<?php

class Model_surat_edaran extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('surat_edaran');
	}

	public function tambah_surat($data, $table){
		$this->db->insert($table, $data);
	}

	public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

		$query = $this->db->query("SELECT file FROM surat_edaran WHERE id_surat_edaran = '$id' ");
		if ($query->num_rows()>0) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}

	public function hapus_data($id)
	{
		$this->db->where('id_surat_edaran', $id);
		return $this->db->delete('surat_edaran');
	}
}
?>