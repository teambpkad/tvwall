<?php

class Model_bg extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('bg');
	}

	public function tambah_bg($data, $table)
	{
		return $this->db->insert($table, $data);
	}

	public function edit_bg($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

		$query = $this->db->query("SELECT foto FROM bg WHERE id_bg = '$id' ");
		if ($query->num_rows()>0) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}

	public function hapus_data($id)
	{
		$this->db->where('id_bg', $id);
		return $this->db->delete('bg');
	}
}
?>